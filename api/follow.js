import * as constants from '../constants';

var followApi = {

  _follow: function(followDetail) {
    return fetch(constants.HTTP_URL + '/mayIFollow', {
      method: 'POST',
      body: JSON.stringify(
        {
          username: followDetail.username,
          followedUser: followDetail.followedUser,
          state: followDetail.state,
        }
      )
    }).then(
      res => res
    ).catch(
      err => console.log(err)
    );
  },

  _checkFollow: function(followDetail) {
    console.log(followDetail.username);
    console.log(followDetail.followedUser);
    return fetch(constants.HTTP_URL + '/checkFollow', {
      method: 'POST',
      body: JSON.stringify(
        {
          username: followDetail.username,
          followedUser: followDetail.followedUser
        }
      )
    }).then(
      res => res
    ).catch(
      err => console.log(err)
    )
  },

  _followingList: function(username) {
    console.log('FOLLOWING: ' + username)
    return fetch(constants.HTTP_URL + '/following', {
      method: 'POST',
      body: JSON.stringify(
        {
          username: username
        }
      )
    }).then(
      res => res
    ).catch(
      err => console.log(err)
    )
  },

  _followerList: function(username) {
    console.log('FOLLOWING: ' + username)
    return fetch(constants.HTTP_URL + '/follower', {
      method: 'POST',
      body: JSON.stringify(
        {
          username: username
        }
      )
    }).then(
      res => res
    ).catch(
      err => console.log(err)
    )
  },

  _fetchHomeDetail: function(username) {
    console.log('FRIEND HOME: ' + username);
    return fetch(constants.HTTP_URL + '/friendHome', {
      method: 'POST',
      body: JSON.stringify(
        {
          username: username
        }
      )
    }).then(
      res => res
    ).catch(
      err => console.log(err)
    )
  }
};

export { followApi as default };
