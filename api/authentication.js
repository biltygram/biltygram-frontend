import * as constants from '../constants';

var authApi = {

  _signup: function(signupForm) {
    console.log(signupForm);
    return fetch(constants.HTTP_URL + '/createUsers', {
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({
        username: signupForm.username,
        password: signupForm.password
      })
    }).then(
      res => res.json()
    ).catch(
      err => console.log(err)
    );
  },

  _signin: async function(signinForm) {
    console.log(signinForm);
    return fetch(constants.HTTP_URL + '/signin', {
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({
        username: signinForm.username,
        password: signinForm.password
      })
    }).then(
      res => res.status
    ).catch(
      err => console.log(err)
    );
  },
};

export { authApi as default };
