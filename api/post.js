import * as constants from '../constants';

var postApi = {

  _homePhoto: function(username) {
    console.log("HOME")
    return fetch(constants.HTTP_URL + '/home', {
      method: 'POST',
      body: JSON.stringify(
        {
          username: username
        }
      )
    }).then(
      (res) => {
        console.log(res)
        return res
      }
    ).catch(
      err => console.log(err)
    );
  },

  _fetchPhoto: function(username) {
    console.log(username);
    return fetch(constants.HTTP_URL + '/retrieve', {
      method: 'POST',
      body: JSON.stringify(
        {
          username: username
        }
      )
    }).then(
      res => res
    ).catch(
      err => console.log(err)
    );
  },

  _postPhoto: function(source) {
    console.log('postPhoto');
    return fetch(constants.HTTP_URL + '/upload', {
      method: 'POST',
      body: JSON.stringify(
        {
          username: source.username,
          uri: 'data=' + encodeURIComponent(source.uri)
        }
      )
    }).then(
      (res) => {
        return res
      }
    ).catch(
      err => console.log(err)
    );
  },

  _getPhotoInfo: function(photoId) {
    return fetch(constants.HTTP_URL + '/photoInfo', {
      method: 'POST',
      body: JSON.stringify(
        {
          photoId: photoId
        }
      )
    }).then(
      res => res
    ).catch(
      err => console.log(err)
    );
  },

  _likePhoto: function(likeDetail) {
    return fetch(constants.HTTP_URL + '/like', {
      method: 'POST',
      body: JSON.stringify(
        {
          username: likeDetail.username,
          photoId: likeDetail.photoId,
          likedByMe: likeDetail.likedByMe,
        }
      )
    });
  },

  _checkLikedByMe: function(username, photoId) {
    return fetch(constants.HTTP_URL + '/likeOrNot', {
      method: 'POST',
      body: JSON.stringify(
        {
          username: username,
          photoId: photoId
        }
      )
    }).then(
      res => res
    ).catch(
      err => console.log(err)
    );
  },

  _whoLikes: function(photoId) {
    return fetch(constants.HTTP_URL + '/whoLikes', {
      method: 'POST',
      body: JSON.stringify(
        {
          photoId: photoId
        }
      )
    }).then(
      res => res
    ).catch(
      err => console.log(err)
    )
  }
};

export { postApi as default };
