import * as constants from '../constants';

var userApi = {

  _follow: function(followPair) {
    return fetch(constants.HTTP_URL + '/follow', {
      method: 'POST',
      body: JSON.stringify(
        {
          username: followPair.username,
          following: followPair.follow
        }
      )
    });
  },

  _unfollow: function(unfollowPair) {
    return fetch(constants.HTTP_URL + '/unfollow', {
      method: 'POST',
      body: JSON.stringify(
        {
          username: unfollowPair.username,
          unfollowing: unfollowPair.unfollow
        }
      )
    });
  }
};

export { postApi as default };
