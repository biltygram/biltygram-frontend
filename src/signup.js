import React from 'react';
import {
  KeyboardAvoidingView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Button from 'apsl-react-native-button';
import Icon from 'react-native-vector-icons/Ionicons';
import DropdownAlert from 'react-native-dropdownalert';
import { Sae } from 'react-native-textinput-effects';
import authApi from '../api/authentication';

const { width, height } = Dimensions.get('window');

export default class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      hidePassword: true,
      iconRevealPassword: 'ios-eye-off',
    };
  }

  static route = {
    navigationBar: {
      tintColor: '#FFFFFF',
      backgroundColor: 'rgba(116, 151, 184, 0.7)',
      translucent: 'true',
    }
  };

  _signup = async () => {
    if (this.state.username == "" || this.state.password == "") {
      console.log('please make sure that no field is left blank')
    } else {
      var signupForm = {
        username: this.state.username,
        password: this.state.password
      };
      var status = await authApi._signup(signupForm);
      if (status == 409) {
        this.dropdown.alertWithType('error', 'Error', 'Account with this username already exists');
      }
    }
  };

  _toggleRevealPassword = () => {
    this.setState({ hidePassword: !this.state.hidePassword });
    this.state.hidePassword ? this.setState({ iconRevealPassword: 'ios-eye' }) : this.setState({ iconRevealPassword: 'ios-eye-off' })
  };

  _loadLogoContainer = () => {
    return (
      <View style={styles.logoContainer}>

      </View>
    )
  };

  _loadFormContainer = () => {
    return (
      <View style={styles.formContainer}>
        <Sae style={styles.textInputStyle}
             label={'Username'}
             labelStyle={styles.textInputLabel}
             iconClass={Icon}
             iconName={'md-mail'}
             iconColor={'#fff'}
             iconSize={20}
             value={this.state.username}
             onChangeText={(username) => this.setState({username})}
             placeholderTextColor="#fff"
             color="#fff"
             autoCapitalize="none"
             autoCorrect={false}
        />
        <View style={{flexDirection: 'row'}}>
          <Sae style={{ height: 40, width: width*0.83, alignItems: 'flex-start' }}
               label={'Password'}
               labelStyle={styles.textInputLabel}
               iconClass={Icon}
               iconName={'md-lock'}
               iconColor={'#fff'}
               iconSize={20}
               value={this.state.password}
               onChangeText={(password) => this.setState({password})}
               secureTextEntry={this.state.hidePassword}
               placeholderTextColor="#fff"
               color="#fff"
               autoCapitalize="none"
               autoCorrect={false}
          />
          <TouchableOpacity onPress={() => this._toggleRevealPassword()}>
            <Icon style={{ paddingLeft: 10, paddingTop: 40 }} name={this.state.iconRevealPassword} color={'#fff'} size={26}/>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  _loadButtonContainer = () => {
    return (
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.buttonOpacityStyle}>
          <Button style={{backgroundColor: '#10B157',borderColor: '#00953B'}} onPress={() => this._signup()}>
            <Text style={styles.buttonText}> Join now</Text>
          </Button>
        </TouchableOpacity>
      </View>
    )
  }

  render () {
    return (
      <View style={styles.container}>
        { this._loadLogoContainer() }
        { this._loadFormContainer() }
        { this._loadButtonContainer() }
        <KeyboardAvoidingView behavior='padding' style={{ paddingTop: 20 }}/>
        <DropdownAlert ref={ref => this.dropdown = ref} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#4EA4DB'
  },
  logoContainer: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  formContainer: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInputStyle: {
    height: 40,
    width: width*0.90,
  },
  textInputLabel: {
    color: '#fff'
  },
  buttonContainer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  logoSize: {
    width: width/4,
    height: width/4,
    margin: 5,
  },
  logoTextSize: {
    resizeMode: 'contain',
  },
  buttonOpacityStyle: {
    width: '85%',
    marginTop: 5,
  },
  buttonText: {
    fontSize: 18,
    color: '#fff',
    fontWeight: 'bold',
  },
});
