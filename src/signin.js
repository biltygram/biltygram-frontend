import React from 'react'
import {
  AsyncStorage,
  Alert,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native'
import Button from 'apsl-react-native-button';
import Icon from 'react-native-vector-icons/Ionicons';
import DropdownAlert from 'react-native-dropdownalert';
import { Sae } from 'react-native-textinput-effects';
import authApi from '../api/authentication';

const { width, height } = Dimensions.get('window');

export default class Signin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: 'billy.coco6',
      password: '1234',
    };
  }

  _signin = async () => {
    let signinForm = {
      username: this.state.username,
      password: this.state.password,
    };
    var status = await authApi._signin(signinForm);
    if (status === 200) {
      await AsyncStorage.setItem('username', signinForm.username);
      this.props.navigator.resetTo({
        screen: 'home',
        title: 'Home'
      })
    } else {
      Alert.alert(
        'Incorrect username/password',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ]
      )
    }
  };

  _signup = () => {
    this.props.navigator.push({
      screen: 'signup',
      navigatorStyle: {
        navBarTextColor: '#fff',
        navBarBackgroundColor: '#1C7CB9',
        navBarButtonColor: '#fff',
      }
    });
  };

  _loadLogoContainer = () => {
    return (
      <View style={styles.logoContainer}>
      </View>
    )
  };

  _loadFormContainer = () => {
    return (
      <View style={styles.formContainer}>
        <Sae style={styles.textInputStyle}
          label={'Username'}
          labelStyle={styles.textInputLabel}
          iconClass={Icon}
          iconName={'md-mail'}
          iconColor={'#fff'}
          iconSize={20}
          value={this.state.username}
          onChangeText={(username) => this.setState({username})}
          placeholderTextColor="#fff"
          color="#fff"
          autoCapitalize="none"
          autoCorrect={false}
          />
        <Sae style={styles.textInputStyle}
          label={'Password'}
          labelStyle={styles.textInputLabel}
          iconClass={Icon}
          iconName={'md-lock'}
          iconColor={'#fff'}
          iconSize={20}
          value={this.state.password}
          onChangeText={(password) => this.setState({password})}
          secureTextEntry={true}
          placeholderTextColor="#fff"
          color="#fff"
          autoCapitalize="none"
          autoCorrect={false}
          />
      </View>
    )
  };

  _loadButtonContainer = () => {
    return (
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.buttonOpacityStyle}>
          <Button style={{borderColor: '#fff'}} onPress={() => this._signin()}>
            <Text style={styles.buttonText}> Login</Text>
          </Button>
        </TouchableOpacity>

        <View style={{flex:1, justifyContent: 'flex-end', alignItems: 'center', marginBottom: 15}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.onScreenText}>Do not have an account?</Text>
            <TouchableOpacity onPress={() => this._signup()}>
              <Text style={styles.signupText}> Sign up </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  };


  render () {
    return (
      <View style={styles.container}>
        { this._loadLogoContainer() }
        { this._loadFormContainer() }
        { this._loadButtonContainer() }
        <DropdownAlert ref={ ref => this.dropdown = ref } />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#4EA4DB'
  },
  logoContainer: {
    flex: 4,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  formContainer: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInputStyle: {
    height: 40,
    width: width*0.90,
  },
  textInputLabel: {
    color: '#fff'
  },
  buttonContainer: {
    flex: 4,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoSize: {
    width: width/4,
    height: width/4,
    margin: 5,
  },
  logoTextSize: {
    resizeMode: 'contain',
  },
  buttonOpacityStyle: {
    width: '85%',
    marginTop: 5,
  },
  buttonText: {
    fontSize: 18,
    color: '#fff',
    fontWeight: 'bold',
  },
  onScreenText: {
    color: '#F8F8F8',
    fontSize: 18,
    fontWeight: '400',
  },
  signupText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '600',
    textDecorationLine: 'underline',
  }
});
