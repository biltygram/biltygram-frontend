import React from 'react'
import {
  AsyncStorage,
  StyleSheet,
  ScrollView,
  Dimensions,
} from 'react-native'
import PostApi from '../api/post';
import FollowApi from '../api/follow';
import User from './component/user';

const { width, height } = Dimensions.get('window');

export default class UserList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      users: [],
    }
  }

  _parseUser = (users) => {
    users = users._bodyInit.substring(5, users._bodyInit.length-1);
    users = users.split(', ');
    this.setState({ users });
  };

  _checkFollow = async () => {
    console.log("CHECKFOLLOW")
    let currentUser = await AsyncStorage.getItem('username');
    let users = [];
    await this.state.users.forEach( async (each) => {
      let isFollow = await FollowApi._checkFollow(
        {
          username: currentUser,
          followedUser: each
        }
      );
      users.push(
        {
          username: each,
          isFollow: isFollow.ok
        }
      )
      this.setState({ users });
    });
  };

  componentDidMount = async () => {
    console.log("HELLO")
    if (this.props.state == 'like') {
      let users = await PostApi._whoLikes(this.props.photoId);
      await this._parseUser(users);
      await this._checkFollow();
    } else if (this.props.state == 'following') {
      console.log("USERLIST FOLLOWING: " + this.props.username);
      let users = await FollowApi._followingList(this.props.username);
      await this._parseUser(users);
      await this._checkFollow();
    } else {
      console.log("USERLIST FOLLOWER: " + this.props.username);
      let users = await FollowApi._followerList(this.props.username);
      await this._parseUser(users);
      await this._checkFollow();
    }
  };

  render () {
    if (this.state.users !== undefined) {
      return (
        <ScrollView style={styles.container}>
          {
            this.state.users.map(user => (
              <User username={user.username} following={user.isFollow} />
            ))
          }
        </ScrollView>
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
