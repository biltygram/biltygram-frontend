import React from 'react';
import {
  AsyncStorage,
  ActivityIndicator,
  StyleSheet,
  View,
  ScrollView,
  Text,
  Dimensions,
} from 'react-native'
import ActionButton from 'react-native-action-button';
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/Ionicons';
import Progress from 'react-native-progress/Bar';
import DropdownAlert from 'react-native-dropdownalert';
import Post from './component/post';
import PostApi from '../api/post';

const { width, height } = Dimensions.get('window');

export default class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      images: [],
      fetching: true,
      uploading: false,
      currentUser: '',
    }
  }

  componentDidMount = async () => {
    let currentUser = await AsyncStorage.getItem('username');
    this.setState({ currentUser });
    this._fetchImages();
  };

  _refresh = async () => {
    let fetching = true;
    this.setState({ fetching });
    await this._fetchImages();
  };

  _fetchImages = async () => {
    console.log("FETCH")
    let fetching = false;
    let res = await PostApi._homePhoto(this.state.currentUser);
    res = res._bodyInit.substring(5, res._bodyInit.length-1);
    res = res.split(', ');
    let images = res.map((v) => {
      let id = v.substring(62).split('?')
      return { id: id[0], src: v }
    });
    console.log(images);
    this.setState({ images });
    this.setState({ fetching });
  };

  _signout = async () => {
    await AsyncStorage.setItem('username', '');
    this.props.navigator.resetTo({
      screen: 'signin',
      navigatorStyle: {
        navBarHidden: true
      }
    })
  };

  _gotoProfileScreen = () => {
    this.props.navigator.push({
      screen: 'profile',
      title: 'Profile',
      passProps: {
        navigatorProps: this.props.navigator,
        username: this.state.currentUser
      }
    })
  };

  _launchCamera = async () => {
    const options = {
        title: 'Upload image',
        cancelButtonTitle: 'Cancel',
        takePhotoButtonTitle: 'Take Photo...',
        chooseFromLibraryButtonTitle: 'Choose from Library...',
        returnBase64Image: true,
        returnIsVertical: false
    };

    ImagePicker.showImagePicker(options, async (response) => {
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        var uploading = true;
        this.setState({ uploading })
        let source = {
          username: this.state.currentUser,
          uri: 'data:image/jpeg;base64,' + response.data
        };
        console.log(response);

        let res = await PostApi._postPhoto(source);
        console.log(res)
        var uploading = false;
        this.setState({ uploading })
        console.log(res.status);
        console.log(res.status == 200);
        if (res.status == 200) {
          this.dropdown.alertWithType('success', 'Done', 'Photo uploaded successfully');
        } else {
          this.dropdown.alertWithType('error', 'Error', 'Could not upload photos');
        }
      }
    })
  }

  _uploading = () => {
    if (this.state.uploading) {
      return (
        <Progress indeterminate={true} width={width} borderRadius={0} />
      )
    }
  }

  _loadCardContainer = () => {
    if (this.state.images.length != 0) {
      return (
        <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
          {
            this.state.images.map(image => (
              <Post photoId={image.id} source={image.src} navigatorProps={this.props.navigator} />
            ))
          }
        </ScrollView>
      )
    }
  };

  _loadActionButton = () => {
    return (
      <ActionButton buttonColor="rgba(231,76,60,1)">
      <ActionButton.Item buttonColor='#E0A531' title="Refresh" onPress={() => this._refresh()}>
        <Icon name="md-refresh" style={styles.actionButtonIcon} />
      </ActionButton.Item>
        <ActionButton.Item buttonColor='#3498db' title="Profile" onPress={() => this._gotoProfileScreen()}>
          <Icon name="md-person" style={styles.actionButtonIcon} />
        </ActionButton.Item>
        <ActionButton.Item buttonColor='#9b59b6' title="Post Images" onPress={() => this._launchCamera()}>
          <Icon name="md-photos" style={styles.actionButtonIcon} />
        </ActionButton.Item>
        <ActionButton.Item buttonColor='#28B58B' title="Sign out" onPress={() => this._signout()}>
          <Icon name="md-log-out" style={styles.actionButtonIcon} />
        </ActionButton.Item>
      </ActionButton>
    )
  };

  render () {
    if (this.state.fetching) {
      return (
        <View style={[styles.container, {justifyContent: 'center', alignItems: 'center'}]}>
          <ActivityIndicator size='large' />
        </View>
      )
    } else {
      return (
        <View style={styles.container}>
          <DropdownAlert ref={ref => this.dropdown = ref} />
          { this._uploading() }
          { this._loadCardContainer() }
          { this._loadActionButton() }
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#f7f7f7",
  },
  actionButtonIcon: {
    color: '#fff',
    fontSize: 20,
  },
});
