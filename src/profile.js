import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StyleSheet,
  View,
  ScrollView,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native'
import ActionButton from 'react-native-action-button';
import Button from 'apsl-react-native-button';
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/Ionicons';
import PhotoGrid from 'react-native-photo-grid';
import PostApi from '../api/post';
import FollowApi from '../api/follow';

const { width, height } = Dimensions.get('window');

export default class Profile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      images: [],
      following: this.props.following,
      ownerProfile: '',
      self: null,
    }
  }

  componentDidMount = async () => {
    let username = await AsyncStorage.getItem('username');
    if (this.props.username === username) {
      let self = true;
      var ownerProfile = username;
      this.setState({ self });
      this.setState({ ownerProfile });
    } else {
      let self = false;
      var ownerProfile = this.props.username;
      this.setState({ self });
      this.setState({ ownerProfile });
    }

    let res = await PostApi._fetchPhoto(this.state.ownerProfile);
    res = res._bodyInit.substring(5, res._bodyInit.length-1);
    res = res.split(', ');
    let images = res.map((v) => {
      let id = v.substring(62).split('?')
      return { id: id[0], src: v }
    });
    this.setState({ images });
    await this._fetchHomeDetail();
  };

  _fetchHomeDetail = async () => {
    let homeDetail = await FollowApi._fetchHomeDetail(this.props.username);
    homeDetail = homeDetail._bodyInit.substring(5, homeDetail._bodyInit.length-1);
    homeDetail = homeDetail.split(', ');
    let followerNumber = homeDetail[0];
    let followingNumber = homeDetail[1];
    console.log(followerNumber);
    console.log(followingNumber);
    this.setState({ followerNumber });
    this.setState({ followingNumber });
  };

  _followPress = async () => {
    console.log("FOLLOW BUTTON");
    let username = await AsyncStorage.getItem('username');
    await FollowApi._follow(
      {
        username: username,
        followedUser: this.state.ownerProfile,
        state: this.props.following,
      }
    );
    var following = !this.state.following;
    this.setState({ following });
  };

  _followerListPress = async () => {
    this.props.navigatorProps.push({
      screen: 'userList',
      title: 'Followers',
      passProps: {
        username: this.props.username,
        state: 'follower'
      }
    });
  };

  _followingListPress = async () => {
    console.log("FOLLOWING LIST PRESS: " + this.props.username)
    this.props.navigatorProps.push({
      screen: 'userList',
      title: 'Following',
      passProps: {
        username: this.props.username,
        state: 'following'
      }
    });
  };

  _renderFollowButton = () => {
    if (!this.state.self) {
      if (this.state.following) {
        return (
          <View style={{ flex:1, marginTop: height/22, marginRight: 10 }}>
            <Button style={{backgroundColor: '#3DAA79',borderColor: '#FFFFFF'}} onPress={() => this._followPress()}>
              <Text style={[styles.buttonText, {color: '#F6F6F6'}]}>Following</Text>
            </Button>
          </View>
        )
      } else {
        return (
          <View style={{ flex:1, marginTop: height/22, marginRight: 10 }}>
            <Button style={{backgroundColor: 'transparent',borderColor: '#7D7D7D'}} onPress={() => this._followPress()}>
              <Text style={[styles.buttonText, {color: '#676767'}]}>Follow +</Text>
            </Button>
          </View>
        )
      }
    }
  };

  _loadProfileContainer = () => {
    return (
      <View style={styles.accountContainer}>
        <View style={styles.profileContainer}>
          <Image
            style={{ width: width/5, height: width/5, margin: 16, borderRadius: width/10 }}
            source={require('../assets/profile.jpg')} />
          <View style={{flex:1, marginTop: height/15, marginLeft: 5}}>
            <Text style={{ fontSize: 18, fontWeight: '400', color: '#323232' }}> { this.props.username } </Text>
          </View>
          { this._renderFollowButton() }
        </View>
        <View style={[styles.profileContainer, {alignItems: 'center'}]}>
          <View style={styles.numberContainer}>
            <Text style={styles.numberText}>{ this.state.images.length }</Text>
            <Text style={styles.descriptionText}>Photos</Text>
          </View>
          <TouchableOpacity style={[styles.numberContainer, {borderLeftWidth: 1, borderRightWidth: 1, borderColor: '#CBCBCB'}]} onPress={() => this._followerListPress()}>
            <Text style={styles.numberText}>{ this.state.followerNumber }</Text>
            <Text style={styles.descriptionText}>Followers</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.numberContainer} onPress={() => this._followingListPress()}>
            <Text style={styles.numberText}>{ this.state.followingNumber }</Text>
            <Text style={styles.descriptionText}>Following</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  };

  _loadCardContainer = () => {
    if (this.state.images !== undefined) {
      // console.log(this.state.images)
      return (
        <View style={styles.imagesContainer}>
          <PhotoGrid
            data = { this.state.images }
            itemsPerRow = { 3 }
            itemMargin = { 1 }
            renderItem = { this.renderItem.bind(this) }
          />
        </View>
      )
    }
  };

  _photoPress = (item) => {
    this.props.navigatorProps.push({
      screen: 'post',
      passProps: {
        navigatorProps: this.props.navigatorProps,
        photoId: item.id,
        source: item.src,
      }
    });
  };

  renderItem(item, itemSize) {
    return(
      <TouchableOpacity
        key={ item.id }
        style={{ width: itemSize, height: itemSize }}
        onPress={() => this._photoPress(item)}>
        <Image
          resizeMode="contain"
          style={{ flex: 1, width: itemSize, height: itemSize }}
          source={{ uri: item.src }}
        />
      </TouchableOpacity>
    )
  };

  render () {
    return (
      <ScrollView style={styles.container}>
        { this._loadProfileContainer() }
        { this._loadCardContainer() }
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF",
  },
  accountContainer: {
    flex: 1,
    height: height/3,
  },
  profileContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    margin: 10,
  },
  imagesContainer: {
    borderTopWidth: 1,
    borderColor: '#CBCBCB',
  },
  buttonText: {
    marginHorizontal: 25,
    fontWeight: '500',
  },
  numberContainer: {
    flex: 1,
    alignItems: 'center',
  },
  numberText: {
    fontSize: 18,
    fontWeight: '500',
  },
  descriptionText: {
    fontSize: 14,
    color: '#5D5D5D',
    marginTop: 2,
  }
});
