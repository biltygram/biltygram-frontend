import React from 'react'
import {
  AsyncStorage,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';
import PostApi from '../../api/post';

const { width, height } = Dimensions.get('window');

export default class Post extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      like: '',
      username: '',
      following: false,
      uri: '',
      likedByMe: false,
    }
  }

  componentDidMount = async () => {
    await this._fetchPhotoDetail(this.props.photoId);
    await this._checkLikedByMe();
  };

  _fetchPhotoDetail = async (photoId) => {
    console.log(photoId)
    let res = await PostApi._getPhotoInfo(photoId);
    res = res._bodyInit.substring(1, res._bodyInit.length-1);
    res = res.split(',')
    let like = res[0];
    let username = res[1];
    this.setState({ like });
    this.setState({ username });
  };

  // check if current user like that photo
  _checkLikedByMe = async () => {
    let username = await AsyncStorage.getItem('username');
    let likedByMe = await PostApi._checkLikedByMe(username, this.props.photoId);
    likedByMe = likedByMe.ok;
    this.setState({ likedByMe });
    console.log(this.state.likedByMe);
  };

  _profilePress = () => {
    
    this.props.navigatorProps.push({
      screen: 'profile',
      passProps: {
        navigatorProps: this.props.navigatorProps,
        following: true,
        username: this.state.username,
      }
    })
  };

  _likePress = async () => {
    let username = await AsyncStorage.getItem('username');
    let likedByMe = !this.state.likedByMe;
    await PostApi._likePhoto(
      {
        username: username,
        photoId: this.props.photoId,
        likedByMe: this.state.likedByMe
      }
    );
    if (this.state.likedByMe) {
      let like = parseInt(this.state.like) - 1
      this.setState({ like })
    } else {
      let like = parseInt(this.state.like) + 1
      this.setState({ like })
    }
    this.setState({ likedByMe });
  };

  _totalLikePress = () => {
    this.props.navigatorProps.push({
      screen: 'userList',
      title: 'Likers',
      passProps: {
        photoId: this.props.photoId,
        state: 'like',
      }
    });
  };

  _renderLikeStatus = () => {
    if (this.state.likedByMe) {
      return (
        <Icon name="ios-heart" size={30} color='#D53D1C' />
      )
    } else {
      return (
        <Icon name="ios-heart-outline" size={30} />
      )
    }
  };

  _loadAccountContainer = () => {
    return (
      <View style={styles.accountContainer}>
        <Image
          style={{ width: width/8, height: width/8, margin: 8, borderRadius: width/16 }}
          source={ require('../../assets/profile.jpg') } />
        <View style={styles.accountTextContainer}>
          <TouchableOpacity onPress={() => this._profilePress()}>
            <Text style={{ fontSize: 15, fontWeight: '500', color: '#3D3D3D' }}> { this.state.username } </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  };

  _loadPhotoContainer = () => {
    return (
      <View style={styles.photoContainer}>
        <Image
          resizeMode="contain"
          style={styles.canvas}
          source={{ uri: this.props.source }} />
      </View>
    )
  };

  _loadLikeContainer = () => {
    return (
      <View style={styles.likeContainer}>
        <TouchableOpacity style={styles.icon} onPress={() => this._likePress()}>
          { this._renderLikeStatus() }
        </TouchableOpacity>
        <View style={styles.likeTextContainer}>
          <TouchableOpacity onPress={() => this._totalLikePress()}>
            <Text>{ this.state.like } likes</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  };


  render () {
    return (
      <View style={styles.container}>
        { this._loadAccountContainer() }
        { this._loadPhotoContainer() }
        { this._loadLikeContainer() }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: height * 0.8,
  },
  accountContainer: {
    flex: 2,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#FFF',
  },
  photoContainer: {
    flex: 10,
    justifyContent: 'center',
    backgroundColor: '#FFF',
    borderColor: '#D5D5D5',
    borderWidth: 1,
    position: 'relative',
  },
  likeContainer: {
    flex: 2,
    flexDirection: 'row',
    backgroundColor: '#FFF'
  },
  accountTextContainer: {
    justifyContent: 'center',
  },
  canvas: {
    flex: 1,
    width: width,
  },
  icon: {
    margin: 10,
    width: 28,
  },
  likeTextContainer: {
    marginTop: height/40
  },
});
