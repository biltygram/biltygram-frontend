import React from 'react'
import {
  AsyncStorage,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native'
import Button from 'apsl-react-native-button';
import FollowApi from '../../api/follow';

const { width, height } = Dimensions.get('window');

export default class Post extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: '',
      following: ''
    }
  }

  componentDidMount = async () => {
    let currentUser = await AsyncStorage.getItem('username');
    let following = this.props.following;
    this.setState({ currentUser });
    this.setState({ following });
  }

  _followPress = async () => {
    console.log("FOLLOW PRESS");
    this._toggleFollow()
    let username = await AsyncStorage.getItem('username');
    await FollowApi._follow(
      {
        username: username,
        followedUser: this.props.username,
        state: this.props.following,
      }
    )
  };

  _renderProfilePicture = () => {
    return (
      <Image
        style={{ width: width/8, height: width/8, margin: 16, borderRadius: width/16 }}
        source={require('../../assets/profile.jpg')} />
    );
  };

  _renderUsername = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Text style={{ marginLeft: 15, fontSize: 18, color: '#454545' }}>{ this.props.username }</Text>
      </View>
    );
  };

  _toggleFollow = () => {
    let following = !this.state.following;
    this.setState({ following });
  };

  _renderFollowButton = () => {
    if (this.state.currentUser != this.props.username) {
      if (this.state.following) {
        return (
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: height/80 }}>
            <Button style={{ width: 120, backgroundColor: '#3DAA79',borderColor: '#FFFFFF'}} onPress={() => this._followPress()}>
              <Text style={{marginHorizontal: 15, color: '#F6F6F6'}}>Following</Text>
            </Button>
          </View>
        )
      } else {
        return (
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: height/80 }}>
            <Button style={{ width: 120, backgroundColor: 'transparent',borderColor: '#7D7D7D'}} onPress={() => this._followPress()}>
              <Text style={{marginHorizontal: 17, color: '#676767'}}>Follow +</Text>
            </Button>
          </View>
        )
      }
    }
  };

  _renderProfile = () => {
    return (
      <View style={{ flex: 1, flexDirection: 'row' }} >
        { this._renderProfilePicture() }
        { this._renderUsername() }
        { this._renderFollowButton() }
      </View>
    )
  };

  render () {
    return (
      <View style={styles.container}>
        { this._renderProfile() }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderBottomWidth: 1,
    borderColor: '#E3E3E3',
  },
});
