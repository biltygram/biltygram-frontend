import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';

const { width, height } = Dimensions.get('window');

export default class ImageGallery extends React.Component {
  constructor(props) {
    super(props);
  }

  _loadPhotoContainer = () => {
    return (
      <View style={styles.photoContainer}>
        <Image
          resizeMode="contain"
          style={styles.canvas}
          source={require('../../assets/sample.jpg')} />
      </View>
    )
  };

  render () {
    return (
      <View style={styles.container}>
        { this._loadPhotoContainer() }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width * 0.3,
  },
  photoContainer: {
    backgroundColor: '#FFF',
    borderColor: '#D5D5D5',
    borderWidth: 1,
    position: 'relative',
  },
  canvas: {
    flex: 1,
    height: width * 0.3,
    width: width * 0.3,
  },
});
