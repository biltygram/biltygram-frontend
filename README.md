# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is the project demo in the final presentation [Biltygram]

### How do I get set up? ###

To run the project, first go to 'main' directory and run 'npm install' to install npm dependencies.
Then, run 'react-native run-ios' to run the project.

### Structure ###

API: (/api/*)
The api which used to send request to backend can be find in this directory

SRC: (/src/*)
The components and views are in this directory

ROUTER: (/router/index.js)
The router used to specify each path is in this directory