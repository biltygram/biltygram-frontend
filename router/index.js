import { Navigation } from 'react-native-navigation';
import SigninScreen from '../src/signin';
import SignupScreen from '../src/signup';
import Home from '../src/home';
import Profile from '../src/profile';
import Post from '../src/component/post';
import UserList from '../src/userList';

export function registerScreens() {
    Navigation.registerComponent('signin', () => SigninScreen);
    Navigation.registerComponent('signup', () => SignupScreen);
    Navigation.registerComponent('home', () => Home);
    Navigation.registerComponent('profile', () => Profile);
    Navigation.registerComponent('post', () => Post);
    Navigation.registerComponent('userList', () => UserList);
}
