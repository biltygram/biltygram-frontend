import React from 'react';
import { Navigation } from 'react-native-navigation';
import { registerScreens } from './router';

registerScreens();

Navigation.startSingleScreenApp({
  screen: {
    screen: 'signin',
    navigatorStyle: {
      navBarHidden: true
    }
  }
});
